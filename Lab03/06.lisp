(defun stablo-trazenja-sirina (graf za-obradu obradjeni)
    (cond   ((null za-obradu) '())
            (t 
                (let* (
                        (nova-obradjeni (append obradjeni (list (car za-obradu))))
                        (potomci-za-obradu (potomci graf (car za-obradu) (append (cdr za-obradu) nova-obradjeni)))
                        (nova-za-obradu (append (cdr za-obradu) potomci-za-obradu))
                      )                      
                      (cons (list (car za-obradu) (potomci graf (car za-obradu) (append (cdr za-obradu) nova-obradjeni))) (stablo-trazenja-sirina graf nova-za-obradu nova-obradjeni))   
                )
            )
    )
)

(defun potomci (graf cvor cvorovi)
    (cond   ((null graf) '())
            ((equal (caar graf) cvor) (filtriraj-cvorove (cadar graf) cvorovi))
            (t (potomci (cdr graf) cvor cvorovi))
    )
)

(defun filtriraj-cvorove (potomci cvorovi)
    (cond   ((null potomci) '())
            ((member (car potomci) cvorovi) (filtriraj-cvorove (cdr potomci) cvorovi))
            (t (cons (car potomci) (filtriraj-cvorove (cdr potomci) cvorovi)))
    )
)

;(stablo-trazenja-sirina '((a (b c)) (b (d e)) (c (e f)) (e (i))) '((a)) '())