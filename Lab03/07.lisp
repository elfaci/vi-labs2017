(defun formiraj-listu (graf pocetni ciljni-heur)
    (--formiraj-listu graf (list pocetni) nil pocetni ciljni-heur)
)

(defun --formiraj-listu (graf za-obradu obradjeni pocetni ciljni-heur)
    (cond   ((null za-obradu) nil)
            (t (let* (
                        (trenutni-cvor (car za-obradu))
                        (nova-obradjeni (append obradjeni (list trenutni-cvor)))
                        (potomci-za-obradu (--izdvoji-potomke graf trenutni-cvor (append za-obradu obradjeni)))
                        (nova-za-obradu (append (cdr za-obradu) potomci-za-obradu)) ;;sirina
                        ;(nova-za-obradu (append potomci-za-obradu (cdr za-obradu))) ;;dubina
                     ) 
                     (cons 
                        (list (--vrati-ceo-cvor graf trenutni-cvor) (--heuristika graf trenutni-cvor ciljni-heur)) 
                        (--formiraj-listu graf nova-za-obradu nova-obradjeni pocetni ciljni-heur)
                     )   
                )
            )
    )
)

(defun --vrati-ceo-cvor (graf cvor)
    (cond   ((null graf) '())
            ((equal (caar graf) cvor) (car graf))
            (t (--vrati-ceo-cvor (cdr graf) cvor))
    )
)

;===== NADJI PUT HEURISTIKA ====
(defun --heuristika (graf pocetak cilj)
    (let (
            (duzina-puta (length (graf-nadji-put graf pocetak cilj)))
         )
         (cond ((= duzina-puta 0) 0)
               (t (1- duzina-puta))
         )
    )
)
   
(defun graf-nadji-put (graf pocetak cilj)
    (--graf-nadji-put graf (list pocetak) cilj '())
)

(defun --graf-nadji-put (graf za-obradu cilj obradjeni)
    (cond   ((null za-obradu) '())
            ((equal (car za-obradu) cilj) (list cilj))
            (t 
                (let* (
                        (nova-obradjeni (append obradjeni (list (car za-obradu))))
                        (potomci-za-obradu (--izdvoji-potomke graf (car za-obradu) (append za-obradu obradjeni)))
                        (nova-za-obradu (append (cdr za-obradu) potomci-za-obradu)) ;;sirina
                        ;(nova-za-obradu (append potomci-za-obradu (cdr za-obradu))) ;;dubina
                        (nadjeni-put (--graf-nadji-put graf nova-za-obradu cilj nova-obradjeni))
                      )
                      (cond ((null nadjeni-put) '())
                            ((member (car nadjeni-put) potomci-za-obradu) (cons (car za-obradu) nadjeni-put))
                            (t nadjeni-put)
                      )
                )
            )
    )
)

(defun --izdvoji-potomke (graf cvor filter-obradjeni)
    (cond   ((null graf) '())
            ((equal (caar graf) cvor) (--filtriraj-cvorove (cadar graf) filter-obradjeni))
            (t (--izdvoji-potomke (cdr graf) cvor filter-obradjeni))
    )
)

(defun --filtriraj-cvorove (potomci filter-obradjeni)
    (cond   ((null potomci) '())
            ((member (car potomci) filter-obradjeni) (--filtriraj-cvorove (cdr potomci) filter-obradjeni))
            (t (cons (car potomci) (--filtriraj-cvorove (cdr potomci) filter-obradjeni)))
    )
)
; ===== /NADJI PUT HEURISTIKA ======

(setq graf0 '((a (b c)) (b (d e)) (c (f g)) (d (h)) (e (i)) (f (j)) (g (j)) (h ()) (i (j)) (j ())))
; (setq graf1 '((a (a b)) (b (c e)) (c (a b)) (d (e f)) (e (d)) (f (a))))
; (setq graf2 '((a (b)) (b (c f)) (c (d)) (d (e)) (e (d)) (f (a))))

(formiraj-listu graf0 'A 'H)
; (formiraj-listu graf1 'A)
; (formiraj-listu graf2 'A)