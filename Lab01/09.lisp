; Zadatak 9 - Vucko

(defun bitp (lista)
	(if (null lista) 0
		(logxor (car lista) (bitp (cdr lista)))
	)
)