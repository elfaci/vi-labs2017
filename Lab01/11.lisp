; Zadatak 11 - Vucko

(defun div (a b)
	(truncate (/ a b))
)

(defun prevedi (broj)
	(if (= broj 0) '()
		(append (prevedi (div broj 2)) (list (mod broj 2)))
	)
)