; Zadatak 7 - Vucko

(defun promeni (n lista)
	(if (null lista) '()
		(append (list (if (>= 0 n) (+ (car lista) 1) (- (car lista) 1))) (promeni (1- n) (cdr lista)))
	)
)
