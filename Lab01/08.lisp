; Zadatak 8 - Vucko

(defun neparni (lista)
	(if (null lista) '()
		(append (list (car lista)) (neparni (cddr lista)))
	)
)