; Zadatak 6 - Vucko

(defun prebroji (lista)
	(if (null lista) '()
		(cons (length (car lista)) (prebroji (cdr lista)))
	)
)