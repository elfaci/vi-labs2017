; Zadatak 14 - Vucko

(defun brparni (lista)
	(if (null lista) 0
		(+ (if (evenp (car lista)) 1 0) (brparni (cdr lista)))
	)
)