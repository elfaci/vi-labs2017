; Zadatak 4 - Vucko

(defun pozitivni (lista)
	(if (null (car lista)) nil
		(append (if (> (car lista) 0) (list (car lista))) (pozitivni (cdr lista)))
	)
)
