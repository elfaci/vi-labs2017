; Zadatak 3 - Vucko

(defun duzina (lista)
	(if (null (car lista)) 0
		(+ 1 (duzina (cdr lista)))
	)
)
