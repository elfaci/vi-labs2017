; Zadatak 12 - Vucko

(defun dodajs (el lista)
	(if (null lista) '()
		(append (if (> el (car lista)) (list (car lista)) (list el (car lista))) (dodajs el (cdr lista)))
	)
)