; Zadatak 13 - Vucko

(defun razmeni (lista)
	(if (= (length lista) 1) (list (car lista))
		(if (null  (cadr lista)) '()
			(append (list (if (< (car lista) (cadr lista)) (cadr lista) (car lista))) (razmeni (if (< (car lista) (cadr lista)) (cons (car lista) (cddr lista)) (cdr lista))))
		)
	)
)