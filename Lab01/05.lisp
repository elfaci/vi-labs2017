; Zadatak 5 - Vucko

(defun zbir (lista)
	(if (null lista) 0
		(+ (car lista) (zbir (cdr lista)))
	)
)