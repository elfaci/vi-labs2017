; Zadatak 15 - Vucko

(defun atomi (lista)
	(if (null lista) '()
		(append (if (atom (car lista)) (list (car lista))) (atomi (cdr lista)))
	)
)