; Zadatak 1 - Vucko

(defun umetni (el n l)
	(if (or (= n 0) (null l))
		(append (list el) l)
		(append (list (car l)) (umetni el (- n 1) (cdr l)))
	)
)
