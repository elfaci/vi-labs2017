; Zadatak 10 - Vucko

(defun progres (n k)
	(if (= n 1) '(1)
		(append (progres (- n 1) k) (list (+ 1 (* (- n 1) k))))
	)
)