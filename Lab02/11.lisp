;;; Zadatak 11 (Lab 2) - Nikola Savic

;;; A - formira palindrom od liste slova ;;;

(defun palindrom (lista) (
    cond ((= (length lista) 1) lista)
         ('T (cons (car lista) (append (palindrom (cdr lista)) (list (car lista)))))
))

(format 't "~a" (palindrom '(a n)))
(format 't "~%~a" (palindrom '(a n a v o l i m)))

;;; B - da li su svi elementi liste numericki ;;;

(defun brojevip (lista) (
    cond ((null lista) 'T) ;zbog and vratimo 'T
         ((listp (car lista)) (and (brojevip (car lista)) (brojevip (cdr lista))))
         ('T (and (numberp (car lista)) (brojevip (cdr lista))))
))

(format 't "~%~a" (brojevip '(1 2 3 a 4 5)))
(format 't "~%~a" (brojevip '((1 2) 3 ((4 (1 2) a) 6 b))))
(format 't "~%~a" (brojevip '((1 2) 3 ((4 (1 2) 7) 6 8))))