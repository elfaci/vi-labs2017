;;;;;;;;;;; Treci zadatak

;;; b

(defun duzina (el)
    (cond 
        ((atom el) 1)
        (t (length el))))

(defun brdugih (n lst)
    (if (null lst) 0
        (+ (if (<= n (duzina (car lst))) 1 0) (brdugih n (cdr lst)))))