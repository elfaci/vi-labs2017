; 3. A, implementirao Stefan

(defun pribavi (prvi-indeks drugi-indeks lista)
    (cond ((> prvi-indeks 0) (pribavi (- prvi-indeks 1) drugi-indeks (cdr lista)))
          ((= prvi-indeks 0) (pribavi (- prvi-indeks 1) drugi-indeks (car lista)))  
          ((> drugi-indeks 0) (pribavi prvi-indeks (- drugi-indeks 1) (cdr lista)))
          (t (append (car lista))))
)

; 3. B

(defun veci-broj (broj lista)
    (if (or (= broj (length lista)) (< broj (length lista))) 1 0)
)

(defun brdrugih (broj lista)
    (if (null lista) 0
        (if (listp (car lista))
            (+ (veci-broj broj (car lista)) (brdrugih broj (cdr lista)))
            (brdrugih broj (cdr lista))
        )
    )
)