#| Zadatak 10. a) |#
(defun napravi (kljucevi vrednosti)
	(cond
		((and (null kljucevi) (null vrednosti)) nil)
		(t (append 
			(list (cons (car kljucevi) (normalizuj-vrednost (car vrednosti)))) 
			(napravi (cdr kljucevi) (cdr vrednosti))))))

(defun normalizuj-vrednost (vrednost)
	(cond
		((null vrednost) nil)
		(t (list vrednost))))

#| Zadatak 10. b) |#
(defun maks (lista)
    (cond 
    	((null lista) nil)
        (t (maks-recursive (car (ispeglaj lista)) (cdr (ispeglaj lista))))))

(defun maks-recursive (m lista)
    (cond
        ((null lista) m)
        ((>= m (car lista)) (maks-recursive m (cdr lista)))
        (t (maks-recursive (car lista) (cdr lista)))))

(defun ispeglaj (lista)
	(cond
		((null lista) nil) 
		((atom lista) (list lista))
		((atom (car lista)) (cons (car lista) (ispeglaj (cdr lista))))
		(t (append (ispeglaj (car lista)) (ispeglaj (cdr lista))))))

; Sa po jednom funkcijom

(defun napravi (kljucevi vrednosti)
    (cond ((null kljucevi) '())
          (t (append (list (cons (car kljucevi) (if (null (car vrednosti)) nil (list (car vrednosti))))) (napravi (cdr kljucevi) (cdr vrednosti))))
    )
)

(defun maks (lista)
    (cond ((= (length lista) 1) (car lista))
          ((atom (car lista)) (if (> (car lista) (maks (cdr lista))) (car lista) (maks (cdr lista))))
          (t (if (> (maks (car lista)) (maks (cdr lista))) (maks (car lista)) (maks (cdr lista))))
    )     
)		