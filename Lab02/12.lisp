#| Zadatak 12. a) |#
(defun razlika (lista1 lista2)
	(cond
		((not (eq (length lista1) (length lista2))) nil)
		((null lista1) nil)
		((null lista2) nil)
		(t (append (list (- (car lista1) (car lista2))) (razlika (cdr lista1) (cdr lista2))))))

#| Zadatak 12. b) |#
(defun obrisi (indeks-pom indeks lista)
	(cond 
		((null lista) nil)
		((eq indeks-pom indeks) (obrisi (1+ indeks-pom) indeks (cdr lista)))
		((atom (car lista)) (cons (car lista) (obrisi (1+ indeks-pom) indeks (cdr lista))))
		((listp (car lista)) (append (list (obrisi 0 indeks (car lista))) (obrisi (1+ indeks-pom) indeks (cdr lista))))
		(t (append (obrisi (1+ indeks-pom) indeks (car lista)) (obrisi (1+ indeks-pom) indeks (cdr lista))))))