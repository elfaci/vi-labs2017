; Zadatak 5, A

(defun nadji-kljuc (lista)
    (list (car lista))
)

(defun kljucevi (lista)
    (if (null lista) '()
        (append (nadji-kljuc (car lista)) (kljucevi (cdr lista)))
    )
)

; Zadatak 5, B

;; 05 - Lab2 - Stefan Sentic

(defun clan-p (el l)
	(cond 
		((null l) '())
		((equal el (car l)) t)
		((listp (car l)) (if (clan-p el (car l)) t (clan-p el (cdr l))))
		(t (clan-p el (cdr l)))
	)
)