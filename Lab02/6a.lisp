;;;;;;;;;;; Šesti zadatak

;;; a

(defun brpon (pom lst)
    (if (null lst) '()
        (append pom (list (bristih lst)) (brpon pom (ostatak lst)))))

(defun bristih (lst)
    (if (or (null lst) (not (equal (car lst) (cadr lst)))) 1
        (+ 1 (bristih (cdr lst)))))

(defun ostatak (lst)
    (if (or (null lst) (not (equal (car lst) (cadr lst)))) (cdr lst)
        (ostatak (cdr lst))))