(defun pomocna (vrsta vektor)
	(cond
		((null vrsta) '0)
		((null vektor) '0)
		(t
			(+
				(* (car vrsta) (car vektor))
				(pomocna (cdr vrsta) (cdr vektor))
			)
		)
	)
)
(defun proizMV (matrica vektor)
	(cond
		((null matrica) '())
		((null vektor) '())
		(t
			(append
				(list (pomocna (car matrica) vektor))
				(proizMV (cdr matrica) vektor)
			)
		)
	)
)
(proizMV '((1 2 3) (4 5 6) (7 8 9)) '(1 2 3))