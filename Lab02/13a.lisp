;;;;;;;;;;; Trinaesti zadatak

;;; PO HORNEROVU SEMU
(defun polinom-horner (x koeficijenti)
    (cond ((null koeficijenti) 0)
          (t (+ (car koeficijenti) (* x (polinom-horner x (cdr koeficijenti)))))
    )
)

;;; a -- KLASICNO (NE HORNER)
(defun polinom (x koeficijenti)
    (polinom-rec x 0 koeficijenti))

(defun polinom-rec (x stepen koeficijenti)
    (if (null koeficijenti) 0
        (+ (* (izracunaj-stepen x stepen) (car koeficijenti)) (polinom-rec x (1+ stepen) (cdr koeficijenti)))))

(defun izracunaj-stepen (x stepen)
    (if (zerop stepen) 1
        (* x (izracunaj-stepen x (1- stepen)))))



(polinom-horner '2 '(3 5 7)) 
(polinom-horner '2 '(6 2 0 7 4 3)) 
