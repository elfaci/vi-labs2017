(defun simetrican (polovina broj)
	(cond
		(
			(not
				(or
					(equal (length polovina) (length broj))
					(equal (length polovina) (- (length broj) 1))
				)
			)
			(simetrican
				(cons
					(car broj)
					polovina
				)
				(cdr broj)
			)
		)
		(t
			(cond
				((equal polovina broj) T)
				((equal polovina (cdr broj)) T)
				(t '())
			)
		)
	)
)
(simetrican '() '(1 2 3 4 5 4 3 2 1))
;(defun simetrican (prazna_lista broj)
;;	(cond
;		()
;	)
;)