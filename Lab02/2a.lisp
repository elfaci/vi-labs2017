(defun zbir (prva druga)
    (cond
        ((null prva) (append druga))
        ((null druga) (append prva))
        (t
            (cons
                (+ (car prva) (car druga))
                (zbir
                    (cdr prva)
                    (cdr druga)
                )
            )
        )
    )
)
(zbir '(1 2 3) '(4 5 ))