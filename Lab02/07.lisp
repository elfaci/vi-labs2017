; Zadatak 7 A

(defun skupi (lista)
    (if (null (cadr lista)) '()
        (append (saberi (car lista) (cadr lista)) (skupi (cdr lista)))
    )
)

(defun saberi (prvi-sabirak drugi-sabirak)
    (list (+ prvi-sabirak drugi-sabirak))
)


#| Druga implementacija |#
#| Zadatak 07. a) |#
(defun skupi (lista)
	(cond 
		((null lista) (list 0))
		((null (cadr lista)) (list (car lista))) 
		(t (skupi-recursive lista))))

(defun skupi-recursive (lista)
	(cond
		((null (cadr lista)) nil)
		(t (append (list (+ (car lista) (cadr lista))) (skupi-recursive (cdr lista))))))

#| Zadatak 07. b) |#
(defun skup-p (lista)
	(skup-recursive (ispeglaj lista)))

(defun skup-recursive (lista)
	(cond 
		((null lista) t)
		(t (and (element-van-liste-p (car lista) (cdr lista)) (skup-recursive (cdr lista))))))

(defun element-van-liste-p (element lista)
	(cond
		((null lista) t)
		(t (and (not (eq element (car lista))) (element-van-liste-p element (cdr lista))))))

(defun ispeglaj (lista)
	(cond
		((null lista) nil) 
		((atom lista) (list lista))
		((atom (car lista)) (cons (car lista) (ispeglaj (cdr lista))))
		(t (append (ispeglaj (car lista)) (ispeglaj (cdr lista))))))
