; 14 zadatak, A

(defun intenzitet (lista)
    (if (null lista) 0
        (+ (* (car lista) (car lista)) (intenzitet (cdr lista)))
    )
)

; 14 zadatak, B

(defun postavi (el prvi-indeks drugi-indeks lista)
    (cond ((> prvi-indeks 0) (postavi el (- prvi-indeks 1) drugi-indeks (cdr lista)))
          (t (setf (nth drugi-indeks (car lista)) el))
    )
)

; Radi ali ne vraća listu, potrebno je definisati listu, pozvati funkciju nad njom a onda odštampati listu.