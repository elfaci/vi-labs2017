8.A)

	(defun podeli (br lista)
		(cond ((null lista) '())
		(t (cons (truncate  (car lista) br) 
				 (if (not (null (cadr lista)))
					(podeli br (cons (+ (* (mod (car lista) br) 10) (cadr lista)) (cddr lista)))
						(podeli br (cdr lista)))))
		)
	)
		
	;;;;;;;;;;;;;;;;;;;;;

	8.B
	(defun unija (l1 l2)
	(format t "~% L1: ~a" l1)
	(format t "~% L2: ~a" l1)
		(cond ((null l2) (list l1) )
		  ((null l1) l2)
			 ((listp l1) 
				(append (unija (car l1) l2) (unija (cdr l1) l2)) )
            (t(if (equal  l1 (car l2)) 
			  '() 
				(unija l1 (cdr l2))))
		)
	)
	(unija '(1 2 3) '(3 4))