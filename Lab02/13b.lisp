;;;;;;;;;;; Trinaesti zadatak

;;; b

(defun proizvod (lst)
    (if (null lst) 1
        (* (if (listp (car lst)) (proizvod-lst (car lst)) (car lst)) (proizvod (cdr lst)))))

(defun proizvod-lst (lst)
    (if (null lst) 1
        (* (car lst) (proizvod-lst (cdr lst)))))