;;;;;;;;;;; Šesti zadatak

;;; b

(defun gldiag (matrix)
    (gldiag-rec 0 0 matrix))

(defun gldiag-rec (i j matrix)
    (if (or (> i (length matrix)) (> j (length matrix))) '()
        (append (pribavi i j matrix) (gldiag-rec (1+ i) (1+ j) matrix))))

(defun pribavi (i j matrix)
    (pribavi-lst j (pribavi-lst i matrix)))

(defun pribavi-lst (n lst) ;;; funkcija radi i kad je element u listi podlista
    (if (> n (length lst)) '())
    (if (zerop n) 
        (if (listp (car lst)) 
            (car lst) 
            (list (car lst)))
            (append '() (pribavi-lst (1- n) (cdr lst)))))