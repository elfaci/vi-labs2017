;;;;;;;;;;; Treci zadatak

;;; a

(defun pribavi (i j matrix)
    (pribavi-lst j (pribavi-lst i matrix)))

(defun pribavi-lst (n lst) ;;; funkcija radi i kad je element u listi podlista
    (if (> n (length lst)) '())
    (if (zerop n) 
        (if (listp (car lst)) 
            (car lst) 
            (list (car lst)))
            (append '() (pribavi-lst (1- n) (cdr lst)))))