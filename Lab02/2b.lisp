(defun broj_pojavljivanja (izraz lista)
	(cond
		((null lista) 0)
		((equal izraz (car lista))
			(+ 1 (broj_pojavljivanja izraz (cdr lista)))
		)
		((listp (car lista))
			(cond
				((equal izraz (caar lista))
					(+ 1 (broj_pojavljivanja izraz 
							(append
								(cdar lista)
								(cdr lista)
							)
						)
					)
				)
				(t
					(broj_pojavljivanja izraz
						(append
							(cdar lista)
							(cdr lista)
						)
					)
				)
			)
		)
		(t 
			(broj_pojavljivanja izraz (cdr lista))
		)
	)
)
(broj_pojavljivanja '(1 2) '((1 2) 3 4 (5 (1 2) (6 (1 2)) 6) 7 (1 2)))

;Stefanova implementacija

(defun broj_pojavljivanja (el lista)
    (cond ((null lista) 0)
          ((equal el (car lista)) (+ 1 (broj_pojavljivanja el (cdr lista))))
          (t (broj_pojavljivanja el (cdr lista))))
)