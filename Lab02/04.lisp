;;; Zadatak 04 (Lab 2) - Nikola Savic

;;; A - salje se kljuc i vraca vrednost ako postoji u a-listi ;;;

;; Vec postoji find f-ja pa baca warning da ce sad da je predefinise

(defun find (kljuc alista) (
    cond ((null alista) '())
         ((equal kljuc (caar alista)) (cadar alista))
         ('T (find kljuc (cdr alista)))
))

(format 't "~a" (find 'a '((a 1) (b 2))))
(format 't "~%~a" (find 'a '((b 2) (c 3))))

;;; B - ubacuje se "el" ispred svakog pojavljivanja "ispredEl" elementa ;;;

(defun insert (el ispredEl lista) (
    cond ((null lista) '())
         ((listp (car lista)) (cons (insert el ispredEl (car lista)) (insert el ispredEl (cdr lista))))
         ((equal ispredEl (car lista)) (cons el (cons (car lista) (insert el ispredEl (cdr lista)))))
         ('T (cons (car lista) (insert el ispredEl (cdr lista))))
))

(format 't "~%~a" (insert 'a 'x '(x y z x w)))
(format 't "~%~a" (insert '(1 2 (3 4)) 'a '(a (b a f) j)))
;(format 't "~%~a" (insert '(1 2 3) 'a '(a (b (a b) a) (a a))))