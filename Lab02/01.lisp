; Zadatak 1b

(defun broj-pojavljivanja-atoma-u-listi (el lista)
    (if (null lista) 0
        (if (= el (car lista)) (+ 1 (broj-pojavljivanja-atoma-u-listi el (cdr lista)))
            (broj-pojavljivanja-atoma-u-listi el (cdr lista))
        )
    )
)

(defun ne-nalazi-se-u-listi (el lista)
    (= (broj-pojavljivanja-atoma-u-listi el lista) 0)
)

(defun ost (prva-lista druga-lista)
    (if (null prva-lista) '()
        (append (if (ne-nalazi-se-u-listi (car prva-lista) druga-lista) (list (car prva-lista))) (ost (cdr prva-lista) druga-lista))
    )
)