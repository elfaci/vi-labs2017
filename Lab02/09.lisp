;;; Zadatak 9 A - Dacha

(defun uporedi (lista)
    (cond ((null lista) 0)
          ((> (caar lista) (cadar lista)) (1+ (uporedi (cdr lista))))
          (t (uporedi (cdr lista)))
    )
)

;(uporedi '((2 1) (1 4) (4 2) (0 2) (3 1))) 
;(uporedi '((1 3) (2 0) (1 1) (2 5) (4 1) (3 3) (1 5))) 



;;; Zadatak 9a B - Dacha

(defun uvecaj (nivo lista)
    (cond ((null lista) nil)
          ((atom (car lista)) (cons (+ nivo (car lista)) (uvecaj nivo (cdr lista))))
          (t (cons (uvecaj (1+ nivo) (car lista)) (uvecaj nivo (cdr lista))))
    )
)

;(uvecaj '0 '(1 2 3 (4 5) 6))
;(uvecaj '0 '((1 2) 3 ((4 (1 2) 5) 6))) 
